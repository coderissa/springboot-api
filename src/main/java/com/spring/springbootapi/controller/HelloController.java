package com.spring.springbootapi.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static com.spring.springbootapi.constants.ApplicationConstants.DEFAULT_RESPONSE_MESSAGE;

@RestController
public class HelloController {

    @RequestMapping("/")
    public String handleHelloWorld() {
        return DEFAULT_RESPONSE_MESSAGE;
    }

    @RequestMapping("/message")
    public String handleHelloWorldwithParam(
            @RequestParam(value = "msg", required = true) String lol
    ) {
        // TODO: Take out extra space
        return "Hello from Mars.  Received: " + lol;
    }
}
